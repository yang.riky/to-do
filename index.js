let todos = [];
let autoId = 1;

function getStatusText(status) {
    let statusText = "On Going";

    if (status) {
        statusText = "Done";
    }

    return statusText;
}

function addTodo(todo) {
    if (todo.name) {
        const defaultTodo = {
            id: autoId,
            name: "",
            status: false
        };

        todo = Object.assign(defaultTodo, todo);

        todos.push(todo);

        autoId++;
    }
}

function getTodoList() {
    console.log(todos);
}

function updateTodoStatus(id) {
    for (let todo of todos) {
        if (id == todo.id) {
            todo.status = !todo.status;
            break;
        }
    }
}

function updateTodoName(id, name) {
    if (name) {
        for (let todo of todos) {
            if (id == todo.id) {
                todo.name = name;
                break;
            }
        }
    }
}

function deleteTodo(id) {
    const filteredTodos = todos.filter(function (todo) {
        return todo.id != id;
    });

    todos = filteredTodos;
}

addTodo({ name: "task 1" });
addTodo({ name: "task 2" });
addTodo({ name: "task 2" });
addTodo({ name: "task 4" });
addTodo({ name: "task 5" });
addTodo({ name: "task 6" });
addTodo({ name: "" });
addTodo({ name: null });
addTodo({});
console.log("List after some todo added:");
getTodoList();

updateTodoStatus(1);
updateTodoName(3, "task 3");
console.log("\n List after some todo updated: ");
getTodoList();

deleteTodo(6);
console.log("\n List after some todo deleted: ");
getTodoList();